///<reference path="../typings/references.d.ts" />
//import ko = require("knockout");
requirejs.config({
    paths: {
        "text": "../lib/require/text",
        "durandal": "../lib/durandal/js",
        "plugins": "../lib/durandal/js/plugins",
        "transitions": "../lib/durandal/js/transitions"
    }
});
// Durandal 2.x assumes no global libraries. It will ship expecting
// Knockout and jQuery to be defined with requirejs. Register jQuery
// and knockout with requirejs as follows:
define("jquery", function () {
    return jQuery;
});
//class KnockoutRequireRegister{
//}
define("knockout", ko);
define(["durandal/system", "durandal/app", "durandal/viewLocator"], function (system, app, viewLocator) {
    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");
    app.title = "Durandal Starter Kit";
    app.configurePlugins({
        router: true,
        dialog: true
    });
    app.start().then(function () {
        //Replace "viewmodels" in the moduleId with "views" to locate the view.
        //Look for partial views in a "views" folder in the root.
        viewLocator.useConvention();
        //Show the app by setting the root view model for our application with a transition.
        app.setRoot("viewmodels/shell", "entrance");
    });
});
//# sourceMappingURL=main.js.map