export class FlickrImage {
    public author: string;
    public author_id: string;
    public date_taken: Date;
    public description: string;
    public link: string;
    public media: {m :string};
    public published: Date;
    public tags: string;
    public title: string;

    constructor(jsonData:any){
        this.author = jsonData.author;
        this.author_id = jsonData.author_id;
        this.date_taken = new Date(jsonData.date_taken);
        this.description = jsonData.description;
        this.link = jsonData.link;
        this.media = jsonData.media;
        this.published = new Date(jsonData.published);
        this.tags = jsonData.tags;
        this.title = jsonData.title;
    }
}