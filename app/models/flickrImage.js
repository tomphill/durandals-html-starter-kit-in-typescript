define(["require", "exports"], function (require, exports) {
    var FlickrImage = (function () {
        function FlickrImage(jsonData) {
            this.author = jsonData.author;
            this.author_id = jsonData.author_id;
            this.date_taken = new Date(jsonData.date_taken);
            this.description = jsonData.description;
            this.link = jsonData.link;
            this.media = jsonData.media;
            this.published = new Date(jsonData.published);
            this.tags = jsonData.tags;
            this.title = jsonData.title;
        }
        return FlickrImage;
    })();
    exports.FlickrImage = FlickrImage;
});
//# sourceMappingURL=flickrImage.js.map