///<reference path="../../typings/references.d.ts" />
define(["require", "exports", "durandal/app", "plugins/router"], function (require, exports, app, router) {
    var Shell = (function () {
        function Shell() {
            this.router = router;
        }
        Shell.prototype.activate = function () {
            router.map([
                { route: "", title: "Welcome", moduleId: "viewmodels/welcome", nav: true },
                { route: "flickr", moduleId: "viewmodels/flickr", nav: true }
            ]).buildNavigationModel();
            return router.activate();
        };
        Shell.prototype.search = function () {
            //It"s really easy to show a message box.
            //You can add custom options too. Also, it returns a promise for the user"s response.
            app.showMessage("Search not yet implemented...");
        };
        return Shell;
    })();
    var vm = new Shell();
    return vm;
});
//# sourceMappingURL=shell.js.map