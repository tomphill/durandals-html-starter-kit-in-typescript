﻿///<reference path="../../typings/references.d.ts" />

import app = require("durandal/app");
import router = require("plugins/router");

class Shell {
    public router: DurandalRootRouter = router;

    constructor(){ }

    public activate():JQueryPromise<any>{
        router.map([
            { route: "", title:"Welcome", moduleId: "viewmodels/welcome", nav: true },
            { route: "flickr", moduleId: "viewmodels/flickr", nav: true }
        ]).buildNavigationModel();

        return router.activate();
    }

    public search():void{
        //It"s really easy to show a message box.
        //You can add custom options too. Also, it returns a promise for the user"s response.
        app.showMessage("Search not yet implemented...");
    }

}

var vm = new Shell();
export = vm;