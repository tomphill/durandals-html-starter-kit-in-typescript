﻿///<reference path="../../typings/references.d.ts" />
import app = require("durandal/app");
import flickrImage = require("models/flickrImage");
import http = require("plugins/http");

class Flickr {
    public displayName:string = "Flickr";
    public images:KnockoutObservableArray<flickrImage.FlickrImage> = ko.observableArray([]);

    constructor() { }

    public activate():JQueryPromise<any> {
        //the router"s activator calls this function and waits for it to complete before proceeding
        if (this.images().length > 0) {
            return null;
        }

        var that:Flickr = this;
        return http.jsonp("http://api.flickr.com/services/feeds/photos_public.gne", {
            tags: "mount ranier",
            tagmode: "any",
            format: "json"
        }, "jsoncallback").then((response:any):void => {
            that.images(ko.utils.arrayMap(response.items, (currentItem:any):flickrImage.FlickrImage=>{
                return new flickrImage.FlickrImage(currentItem);
            }));
        });
    }

    public canDeactivate():JQueryPromise<any> {
        //the router"s activator calls this function to see if it can leave the screen
        return app.showMessage("Are you sure you want to leave this page?", "Navigate", ["Yes", "No"]);
    }

    public select(item: flickrImage.FlickrImage):void {
        //the app model allows easy display of modal dialogs by passing a view model
        //views are usually located by convention, but you an specify it as well with viewUrl
        app.showDialog($.extend(item, {viewUrl: "views/detail"}));
    }
}

//Note: This module exports an object.
//That means that every module that "requires" it will get the same object instance.
//If you wish to be able to create multiple instances, instead export a function.
//See the "welcome" module for an example of function export.
var vm = new Flickr();
export = vm;