define(["require", "exports", "durandal/app", "models/flickrImage", "plugins/http"], function (require, exports, app, flickrImage, http) {
    var Flickr = (function () {
        function Flickr() {
            this.displayName = "Flickr";
            this.images = ko.observableArray([]);
        }
        Flickr.prototype.activate = function () {
            //the router"s activator calls this function and waits for it to complete before proceeding
            if (this.images().length > 0) {
                return null;
            }
            var that = this;
            return http.jsonp("http://api.flickr.com/services/feeds/photos_public.gne", {
                tags: "mount ranier",
                tagmode: "any",
                format: "json"
            }, "jsoncallback").then(function (response) {
                that.images(ko.utils.arrayMap(response.items, function (currentItem) {
                    return new flickrImage.FlickrImage(currentItem);
                }));
            });
        };
        Flickr.prototype.canDeactivate = function () {
            //the router"s activator calls this function to see if it can leave the screen
            return app.showMessage("Are you sure you want to leave this page?", "Navigate", ["Yes", "No"]);
        };
        Flickr.prototype.select = function (item) {
            //the app model allows easy display of modal dialogs by passing a view model
            //views are usually located by convention, but you an specify it as well with viewUrl
            app.showDialog($.extend(item, { viewUrl: "views/detail" }));
        };
        return Flickr;
    })();
    //Note: This module exports an object.
    //That means that every module that "requires" it will get the same object instance.
    //If you wish to be able to create multiple instances, instead export a function.
    //See the "welcome" module for an example of function export.
    var vm = new Flickr();
    return vm;
});
//# sourceMappingURL=flickr.js.map